//
//  TPAppDelegate.m
//  InstaPokemon
//
//  Created by ellisa on 7/7/14.
//  Copyright (c) 2014 Ellisa. All rights reserved.
//

#import "TPAppDelegate.h"
#import <AVFoundation/AVAudioPlayer.h>
#import <AudioToolbox/AudioServices.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "ALSdk.h"
#import "ALInterstitialAd.h"
#import "Flurry.h"
#import "TPGlobal.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>




@interface TPAppDelegate()
@end

@implementation TPAppDelegate

bool isStartUp = YES;
@synthesize window;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    if([[UIDevice currentDevice].systemVersion floatValue]>=8.0){
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
        if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]){
            [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
        }
#endif
        
    }
    
#ifdef FreeApp
    //if (!AppSettings::getBoolValue(Buy_RemoveAds))
    //{
    
        // Optional: automatically send uncaught exceptions to Google Analytics.
        //[GAI sharedInstance].trackUncaughtExceptions = YES;
        
        // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
        //[GAI sharedInstance].dispatchInterval = 20;
        
        // Optional: set Logger to VERBOSE for debug information.
        //[[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
        
        // Initialize tracker. Replace with your tracking ID.
        //[[GAI sharedInstance] trackerWithTrackingId:@"UA-57527220-3"];

        [self buttonGridTapped];
   // }
#endif
    // Override point for customization after application launch.
    
//    splashImage = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, 320, 480)];
//    splashImage.image = [UIImage imageNamed:@"Default.png"];
//    [window addSubview:splashImage];
//    [window bringSubviewToFront:splashImage];
// 
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:3];
//    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:window cache:YES];
//    [UIView setAnimationDelegate:self];
//    [UIView setAnimationWillStartSelector:@selector(playSound:finished:context:)];
//    [UIView setAnimationDidStopSelector:@selector(startupAnimationDone:finished:context:)];
//    splashImage.alpha = 0.0;
//    [UIView commitAnimations];
//    [TPGlobal setPurchased];
    
    [self playsound];
    
    sleep(2.5f);
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    // Add any custom logic here.
    [ALSdk initializeSdk];
    //configure iRate
    [iRate sharedInstance].appStoreID = 1178684921; // Replace this
    [iRate sharedInstance].daysUntilPrompt = 3;
    [iRate sharedInstance].usesUntilPrompt = 3;
    //[iRate sharedInstance].previewMode = YES;
    //[iRate sharedInstance].verboseLogging = YES;
    

    //flurry
    FlurrySessionBuilder* builder = [[[[[FlurrySessionBuilder new]
                                        withLogLevel:FlurryLogLevelAll]
                                       withCrashReporting:YES]
                                      withSessionContinueSeconds:10]
                                     withAppVersion:@"0.1.2"];
    
    [Flurry startSession:@"VGW5XGQVJV8FFSD8HWBK" withSessionBuilder:builder];
    [Flurry logEvent:@"logEvent: app just started"];
    
    //crashlytics
    [Fabric with:@[[Crashlytics class]]];
    
    return YES;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                               annotation:options[UIApplicationOpenURLOptionsAnnotationKey]
                    ];
    // Add any custom logic here.
    return handled;
}


- (void)buttonGridTapped
{

        [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(buttonGridTapped) userInfo:nil repeats:NO];
}

							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Logs 'install' and 'app activate' App Events.
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    // Begin a user session. Must not be dependent on user actions or any prior network requests.
    // Must be called every time your app becomes active.
      [FBSDKAppEvents activateApp];
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


static SystemSoundID soundID;

- (void)playsound
{
    if (soundID == 0)
    {
        NSString *path = [NSString stringWithFormat:@"%@/sound.mp3",[[NSBundle mainBundle] resourcePath]];
        NSURL *filePath = [NSURL fileURLWithPath:path isDirectory:NO];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef) filePath, &soundID);
    }
    
    AudioServicesPlaySystemSound(soundID);
}

- (void)playSound:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
//    NSURL *soundURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Growling ab" ofType:@"mp3"]];
//    
//    AVAudioPlayer *audioPlayer = [[AVAudioPlayer  alloc] initWithContentsOfURL:soundURL error:nil];
//    [audioPlayer play];
    
    if (soundID == 0)
    {
        NSString *path = [NSString stringWithFormat:@"%@/sound.mp3",[[NSBundle mainBundle] resourcePath]];
        NSURL *filePath = [NSURL fileURLWithPath:path isDirectory:NO];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef) filePath, &soundID);
    }
    
    AudioServicesPlaySystemSound(soundID);
    
}

- (void)startupAnimationDone:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
{
    
}


@end

//
//  TPViewController.h
//  InstaPokemon
//
//  Created by ellisa on 7/7/14.
//  Copyright (c) 2014 Ellisa. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleMobileAds;


@interface TPViewController : UIViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{
    
    UIImagePickerController *_picker;
    GADBannerView *bannerView_;
    BOOL areAdsRemoved;


    
    IBOutlet UIImageView *imageViewForPimp;
    IBOutlet UIButton *removeAdsbtn;
    
    IBOutlet UIImageView *imageViewForTitle;
    

}
- (IBAction)cameraAction:(id)sender;
- (IBAction)showAlbumAction:(id)sender;
- (IBAction)removeAdsAction:(id)sender;

- (IBAction)connectWithFB:(id)sender;

@end
